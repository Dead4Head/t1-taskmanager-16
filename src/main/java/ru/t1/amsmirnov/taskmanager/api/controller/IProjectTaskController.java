package ru.t1.amsmirnov.taskmanager.api.controller;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface IProjectTaskController {

    void bindTaskToProject() throws AbstractException;

    void unbindTaskToProject() throws AbstractException;

}
