package ru.t1.amsmirnov.taskmanager.api.controller;

import ru.t1.amsmirnov.taskmanager.exception.AbstractException;

public interface ITaskController {

    void createTask() throws AbstractException;

    void showTasks();

    void showTaskById() throws AbstractException;

    void showTaskByIndex() throws AbstractException;

    void showTaskByProjectId() throws AbstractException;

    void updateTaskById() throws AbstractException;

    void updateTaskByIndex() throws AbstractException;

    void changeTaskStatusById() throws AbstractException;

    void changeTaskStatusByIndex() throws AbstractException;

    void startTaskById() throws AbstractException;

    void startTaskByIndex() throws AbstractException;

    void completeTaskById() throws AbstractException;

    void completeTaskByIndex() throws AbstractException;

    void clearTasks() throws AbstractException;

    void removeTasksById() throws AbstractException;

    void removeTasksByIndex() throws AbstractException;

}